export const person = 
[
    {
        "adult": false,
        "gender": 1,
        "id": 1245,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/kwUQFeFXOOpgloMgZaadhzkbTI4.jpg",
                "genre_ids": [
                    878,
                    28,
                    12
                ],
                "id": 24428,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Avengers",
                "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
                "poster_path": "/RYMX2wcKCBAr24UyPD7xwmjaTn.jpg",
                "release_date": "2012-04-25",
                "title": "The Avengers",
                "video": false,
                "vote_average": 7.7,
                "vote_count": 23839
            },
            {
                "adult": false,
                "backdrop_path": "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
                "genre_ids": [
                    12,
                    28,
                    878
                ],
                "id": 299536,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Avengers: Infinity War",
                "overview": "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
                "poster_path": "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
                "release_date": "2018-04-25",
                "title": "Avengers: Infinity War",
                "video": false,
                "vote_average": 8.3,
                "vote_count": 20677
            },
            {
                "adult": false,
                "backdrop_path": "/lwLVC62xz5sEY4ECUYJ5BjtcBlP.jpg",
                "genre_ids": [
                    12,
                    28,
                    878
                ],
                "id": 271110,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Captain America: Civil War",
                "overview": "Following the events of Age of Ultron, the collective governments of the world pass an act designed to regulate all superhuman activity. This polarizes opinion amongst the Avengers, causing two factions to side with Iron Man or Captain America, which causes an epic battle between former allies.",
                "poster_path": "/rAGiXaUfPzY7CDEyNKUofk3Kw2e.jpg",
                "release_date": "2016-04-27",
                "title": "Captain America: Civil War",
                "video": false,
                "vote_average": 7.4,
                "vote_count": 17347
            }
        ],
        "known_for_department": "Acting",
        "name": "Scarlett Johansson",
        "popularity": 45.948,
        "profile_path": "/6NsMbJXRlDZuDzatN2akFdGuTvx.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 976,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/mRfI3y2oAd7ejur2di09xC9niqp.jpg",
                "genre_ids": [
                    28,
                    53,
                    80
                ],
                "id": 82992,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Fast & Furious 6",
                "overview": "Hobbs has Dominic and Brian reassemble their crew to take down a team of mercenaries: Dominic unexpectedly gets convoluted also facing his presumed deceased girlfriend, Letty.",
                "poster_path": "/n31VRDodbaZxkrZmmzyYSFNVpW5.jpg",
                "release_date": "2013-05-21",
                "title": "Fast & Furious 6",
                "video": false,
                "vote_average": 6.8,
                "vote_count": 8362
            },
            {
                "adult": false,
                "backdrop_path": "/vkQvqH8WQkUiNklDcbUtHEUgSNQ.jpg",
                "genre_ids": [
                    28,
                    53
                ],
                "id": 168259,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Furious 7",
                "overview": "Deckard Shaw seeks revenge against Dominic Toretto and his family for his comatose brother.",
                "poster_path": "/ktofZ9Htrjiy0P6LEowsDaxd3Ri.jpg",
                "release_date": "2015-04-01",
                "title": "Furious 7",
                "video": false,
                "vote_average": 7.3,
                "vote_count": 8045
            },
            {
                "adult": false,
                "backdrop_path": "/jzdnhRhG0dsuYorwvSqPqqnM1cV.jpg",
                "genre_ids": [
                    28,
                    80,
                    53
                ],
                "id": 337339,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Fate of the Furious",
                "overview": "When a mysterious woman seduces Dom into the world of crime and a betrayal of those closest to him, the crew face trials that will test them as never before.",
                "poster_path": "/dImWM7GJqryWJO9LHa3XQ8DD5NH.jpg",
                "release_date": "2017-04-12",
                "title": "The Fate of the Furious",
                "video": false,
                "vote_average": 6.9,
                "vote_count": 7916
            }
        ],
        "known_for_department": "Acting",
        "name": "Jason Statham",
        "popularity": 38.685,
        "profile_path": "/lldeQ91GwIVff43JBrpdbAAeYWj.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 90633,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/6iUNJZymJBMXXriQyFZfLAKnjO6.jpg",
                "genre_ids": [
                    28,
                    12,
                    14
                ],
                "id": 297762,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Wonder Woman",
                "overview": "An Amazon princess comes to the world of Man in the grips of the First World War to confront the forces of evil and bring an end to human conflict.",
                "poster_path": "/gfJGlDaHuWimErCr5Ql0I8x9QSy.jpg",
                "release_date": "2017-05-30",
                "title": "Wonder Woman",
                "video": false,
                "vote_average": 7.3,
                "vote_count": 15813
            },
            {
                "adult": false,
                "backdrop_path": "/doiUtOHzcxXFl0GVQ2n8Ay6Pirx.jpg",
                "genre_ids": [
                    28,
                    12,
                    14
                ],
                "id": 209112,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Batman v Superman: Dawn of Justice",
                "overview": "Fearing the actions of a god-like Super Hero left unchecked, Gotham City’s own formidable, forceful vigilante takes on Metropolis’s most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it’s ever known before.",
                "poster_path": "/5UsK3grJvtQrtzEgqNlDljJW96w.jpg",
                "release_date": "2016-03-23",
                "title": "Batman v Superman: Dawn of Justice",
                "video": false,
                "vote_average": 5.9,
                "vote_count": 14186
            },
            {
                "adult": false,
                "backdrop_path": "/jorgjEk6a0bed6jdR5wu4S6ZvRm.jpg",
                "genre_ids": [
                    28,
                    12,
                    14,
                    878
                ],
                "id": 141052,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Justice League",
                "overview": "Fuelled by his restored faith in humanity and inspired by Superman's selfless act, Bruce Wayne and Diana Prince assemble a team of metahumans consisting of Barry Allen, Arthur Curry and Victor Stone to face the catastrophic threat of Steppenwolf and the Parademons who are on the hunt for three Mother Boxes on Earth.",
                "poster_path": "/eifGNCSDuxJeS1loAXil5bIGgvC.jpg",
                "release_date": "2017-11-15",
                "title": "Justice League",
                "video": false,
                "vote_average": 6.2,
                "vote_count": 10019
            }
        ],
        "known_for_department": "Acting",
        "name": "Gal Gadot",
        "popularity": 36.48,
        "profile_path": "/xu1l9WmNY1XYZyJ3M2gvGqCCDGS.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 1734,
        "known_for": [
            {
                "backdrop_path": "/mZjZgY6ObiKtVuKVDrnS9VnuNlE.jpg",
                "first_air_date": "2017-09-25",
                "genre_ids": [
                    18
                ],
                "id": 71712,
                "media_type": "tv",
                "name": "The Good Doctor",
                "origin_country": [
                    "US"
                ],
                "original_language": "en",
                "original_name": "The Good Doctor",
                "overview": "A young surgeon with Savant syndrome is recruited into the surgical unit of a prestigious hospital. The question will arise: can a person who doesn't have the ability to relate to people actually save their lives",
                "poster_path": "/6tfT03sGp9k4c0J3dypjrI8TSAI.jpg",
                "vote_average": 8.6,
                "vote_count": 6392
            },
            {
                "backdrop_path": "/1ot3DeWBHaI3cbfQtqZqOI1VoPI.jpg",
                "first_air_date": "1990-11-18",
                "genre_ids": [
                    18,
                    9648,
                    10765
                ],
                "id": 19614,
                "media_type": "tv",
                "name": "It",
                "origin_country": [
                    "US"
                ],
                "original_language": "en",
                "original_name": "It",
                "overview": "In 1960, seven outcast kids known as \"The Losers' Club\" fight an evil demon who poses as a child-killing clown. Thirty years later, they reunite to stop the demon once and for all when it returns to their hometown.",
                "poster_path": "/4ybQ6gopB3H3cu0seVZLznDnIKo.jpg",
                "vote_average": 6.8,
                "vote_count": 2047
            },
            {
                "backdrop_path": "/kuOt3LxmHW8k39SK2tyg6qSWhop.jpg",
                "first_air_date": "2001-10-16",
                "genre_ids": [
                    10759,
                    10765,
                    18
                ],
                "id": 4604,
                "media_type": "tv",
                "name": "Smallville",
                "origin_country": [
                    "US"
                ],
                "original_language": "en",
                "original_name": "Smallville",
                "overview": "The origins of the world’s greatest hero–from Krypton refugee Kal-el’s arrival on Earth through his tumultuous teen years to Clark Kent’s final steps toward embracing his destiny as the Man of Steel.",
                "poster_path": "/pUhJGETy2sec4vEkiqJ9eGeIywc.jpg",
                "vote_average": 8.1,
                "vote_count": 1984
            }
        ],
        "known_for_department": "Acting",
        "name": "Annette O'Toole",
        "popularity": 34.328,
        "profile_path": "/z5BS5EeuhpM2KGFfUjOhVXtjmMy.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 31,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/tlEFuIlaxRPXIYVHXbOSAMCfWqk.jpg",
                "genre_ids": [
                    35,
                    18,
                    10749
                ],
                "id": 13,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Forrest Gump",
                "overview": "A man with a low IQ has accomplished great things in his life and been present during significant historic events—in each case, far exceeding what anyone imagined he could do. But despite all he has achieved, his one true love eludes him.",
                "poster_path": "/h5J4W4veyxMXDMjeNxZI46TsHOb.jpg",
                "release_date": "1994-07-06",
                "title": "Forrest Gump",
                "video": false,
                "vote_average": 8.5,
                "vote_count": 19716
            },
            {
                "adult": false,
                "backdrop_path": "/3Rfvhy1Nl6sSGJwyjb0QiZzZYlB.jpg",
                "genre_ids": [
                    16,
                    12,
                    10751,
                    35
                ],
                "id": 862,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Toy Story",
                "overview": "Led by Woody, Andy's toys live happily in his room until Andy's birthday brings Buzz Lightyear onto the scene. Afraid of losing his place in Andy's heart, Woody plots against Buzz. But when circumstances separate Buzz and Woody from their owner, the duo eventually learns to put aside their differences.",
                "poster_path": "/uXDfjJbdP4ijW5hWSBrPrlKpxab.jpg",
                "release_date": "1995-10-30",
                "title": "Toy Story",
                "video": false,
                "vote_average": 7.9,
                "vote_count": 13437
            },
            {
                "adult": false,
                "backdrop_path": "/xMIyotorUv2Yz7zpQz2QYc8wkWB.jpg",
                "genre_ids": [
                    14,
                    18,
                    80
                ],
                "id": 497,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Green Mile",
                "overview": "A supernatural tale set on death row in a Southern prison, where gentle giant John Coffey possesses the mysterious power to heal people's ailments. When the cell block's head guard, Paul Edgecomb, recognizes Coffey's miraculous gift, he tries desperately to help stave off the condemned man's execution.",
                "poster_path": "/velWPhVMQeQKcxggNEU8YmIo52R.jpg",
                "release_date": "1999-12-10",
                "title": "The Green Mile",
                "video": false,
                "vote_average": 8.5,
                "vote_count": 11584
            }
        ],
        "known_for_department": "Acting",
        "name": "Tom Hanks",
        "popularity": 31.628,
        "profile_path": "/zVyoR2ZNiVGymjZK4MMJHIdpxoW.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 1813,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/xJHokMbljvjADYdit5fK5VQsXEG.jpg",
                "genre_ids": [
                    12,
                    18,
                    878
                ],
                "id": 157336,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Interstellar",
                "overview": "The adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.",
                "poster_path": "/gEU2QniE6E77NI6lCU6MxlNBvIx.jpg",
                "release_date": "2014-11-05",
                "title": "Interstellar",
                "video": false,
                "vote_average": 8.3,
                "vote_count": 24772
            },
            {
                "adult": false,
                "backdrop_path": "/cKPfiu9IcCW0fMdKdQBXe3PRtTZ.jpg",
                "genre_ids": [
                    28,
                    80,
                    18,
                    53
                ],
                "id": 49026,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Dark Knight Rises",
                "overview": "Following the death of District Attorney Harvey Dent, Batman assumes responsibility for Dent's crimes to protect the late attorney's reputation and is subsequently hunted by the Gotham City Police Department. Eight years later, Batman encounters the mysterious Selina Kyle and the villainous Bane, a new terrorist leader who overwhelms Gotham's finest. The Dark Knight resurfaces to protect a city that has branded him an enemy.",
                "poster_path": "/vzvKcPQ4o7TjWeGIn0aGC9FeVNu.jpg",
                "release_date": "2012-07-16",
                "title": "The Dark Knight Rises",
                "video": false,
                "vote_average": 7.7,
                "vote_count": 17209
            },
            {
                "adult": false,
                "backdrop_path": "/20pkC7yJdCV4J1IMKfsCT9QU7zV.jpg",
                "genre_ids": [
                    10751,
                    14,
                    12
                ],
                "id": 12155,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Alice in Wonderland",
                "overview": "Alice, an unpretentious and individual 19-year-old, is betrothed to a dunce of an English nobleman. At her engagement party, she escapes the crowd to consider whether to go through with the marriage and falls down a hole in the garden after spotting an unusual rabbit. Arriving in a strange and surreal place called 'Underland,' she finds herself in a world that resembles the nightmares she had as a child, filled with talking animals, villainous queens and knights, and frumious bandersnatches. Alice realizes that she is there for a reason – to conquer the horrific Jabberwocky and restore the rightful queen to her throne.",
                "poster_path": "/o0kre9wRCZz3jjSjaru7QU0UtFz.jpg",
                "release_date": "2010-03-03",
                "title": "Alice in Wonderland",
                "video": false,
                "vote_average": 6.6,
                "vote_count": 10922
            }
        ],
        "known_for_department": "Acting",
        "name": "Anne Hathaway",
        "popularity": 29.132,
        "profile_path": "/tLelKoPNiyJCSEtQTz1FGv4TLGc.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 1397778,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/9pkZesKMnblFfKxEhQx45YQ2kIe.jpg",
                "genre_ids": [
                    27,
                    53
                ],
                "id": 381288,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Split",
                "overview": "Though Kevin has evidenced 23 personalities to his trusted psychiatrist, Dr. Fletcher, there remains one still submerged who is set to materialize and dominate all the others. Compelled to abduct three teenage girls led by the willful, observant Casey, Kevin reaches a war for survival among all of those contained within him — as well as everyone around him — as the walls between his compartments shatter apart.",
                "poster_path": "/bqb9WsmZmDIKxqYmBJ9lj7J6hzn.jpg",
                "release_date": "2017-01-09",
                "title": "Split",
                "video": false,
                "vote_average": 7.3,
                "vote_count": 13314
            },
            {
                "adult": false,
                "backdrop_path": "/ngBFDOsx13sFXiMweDoL54XYknR.jpg",
                "genre_ids": [
                    53,
                    18,
                    878
                ],
                "id": 450465,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Glass",
                "overview": "In a series of escalating encounters, former security guard David Dunn uses his supernatural abilities to track Kevin Wendell Crumb, a disturbed man who has twenty-four personalities. Meanwhile, the shadowy presence of Elijah Price emerges as an orchestrator who holds secrets critical to both men.",
                "poster_path": "/svIDTNUoajS8dLEo7EosxvyAsgJ.jpg",
                "release_date": "2019-01-16",
                "title": "Glass",
                "video": false,
                "vote_average": 6.6,
                "vote_count": 5889
            },
            {
                "adult": false,
                "backdrop_path": "/n2UXRCLkxtukYGXYYKEIGrm6zJn.jpg",
                "genre_ids": [
                    27,
                    18,
                    9648,
                    36
                ],
                "id": 310131,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Witch",
                "overview": "In 1630s New England, William and Katherine lead a devout Christian life with five children, homesteading on the edge of an impassable wilderness, exiled from their settlement when William defies the local church. When their newborn son vanishes and crops mysteriously fail, the family turns on one another.",
                "poster_path": "/o384OC1tCV8nCnozkM6y4MiSqsw.jpg",
                "release_date": "2015-01-27",
                "title": "The Witch",
                "video": false,
                "vote_average": 6.8,
                "vote_count": 4376
            }
        ],
        "known_for_department": "Acting",
        "name": "Anya Taylor-Joy",
        "popularity": 28.819,
        "profile_path": "/1zA5GuTcciupj4PsNoV2YemB0Wm.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 109513,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/mMoG4nPSDupXIXOwVvpexZY2W0N.jpg",
                "genre_ids": [
                    28,
                    18,
                    53
                ],
                "id": 254128,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "San Andreas",
                "overview": "In the aftermath of a massive earthquake in California, a rescue-chopper pilot makes a dangerous journey across the state in order to rescue his estranged daughter.",
                "poster_path": "/2Gfjn962aaFSD6eST6QU3oLDZTo.jpg",
                "release_date": "2015-05-27",
                "title": "San Andreas",
                "video": false,
                "vote_average": 6.2,
                "vote_count": 6356
            },
            {
                "adult": false,
                "backdrop_path": "/6QmX2BDVr1hIOIPHqnxvp1C1ZZp.jpg",
                "genre_ids": [
                    28,
                    35,
                    80
                ],
                "id": 339846,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Baywatch",
                "overview": "Devoted lifeguard Mitch Buchannon butts heads with a brash new recruit. Together, they uncover a local criminal plot that threatens the future of the Bay.",
                "poster_path": "/6HE4xd8zloDqmjMZuhUCCw2UcY1.jpg",
                "release_date": "2017-05-12",
                "title": "Baywatch",
                "video": false,
                "vote_average": 6.1,
                "vote_count": 6159
            },
            {
                "adult": false,
                "backdrop_path": "/sEyNWq9985lyUrwayWnvicT4FHA.jpg",
                "genre_ids": [
                    12,
                    14,
                    10751
                ],
                "id": 32657,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Percy Jackson & the Olympians: The Lightning Thief",
                "overview": "Accident prone teenager, Percy discovers he's actually a demi-God, the son of Poseidon, and he is needed when Zeus' lightning is stolen. Percy must master his new found skills in order to prevent a war between the Gods that could devastate the entire world.",
                "poster_path": "/brzpTyZ5bnM7s53C1KSk1TmrMO6.jpg",
                "release_date": "2010-02-01",
                "title": "Percy Jackson & the Olympians: The Lightning Thief",
                "video": false,
                "vote_average": 6.2,
                "vote_count": 5556
            }
        ],
        "known_for_department": "Acting",
        "name": "Alexandra Daddario",
        "popularity": 28.132,
        "profile_path": "/lh5zibQXYH1MNqkuX8TmxyNYHhK.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 550843,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
                "genre_ids": [
                    12,
                    28,
                    878
                ],
                "id": 299536,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Avengers: Infinity War",
                "overview": "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
                "poster_path": "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
                "release_date": "2018-04-25",
                "title": "Avengers: Infinity War",
                "video": false,
                "vote_average": 8.3,
                "vote_count": 20677
            },
            {
                "adult": false,
                "backdrop_path": "/lwLVC62xz5sEY4ECUYJ5BjtcBlP.jpg",
                "genre_ids": [
                    12,
                    28,
                    878
                ],
                "id": 271110,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Captain America: Civil War",
                "overview": "Following the events of Age of Ultron, the collective governments of the world pass an act designed to regulate all superhuman activity. This polarizes opinion amongst the Avengers, causing two factions to side with Iron Man or Captain America, which causes an epic battle between former allies.",
                "poster_path": "/rAGiXaUfPzY7CDEyNKUofk3Kw2e.jpg",
                "release_date": "2016-04-27",
                "title": "Captain America: Civil War",
                "video": false,
                "vote_average": 7.4,
                "vote_count": 17347
            },
            {
                "adult": false,
                "backdrop_path": "/8i6ZDk97Vyh0jHohMG4vFeFuKJh.jpg",
                "genre_ids": [
                    28,
                    12,
                    878
                ],
                "id": 99861,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Avengers: Age of Ultron",
                "overview": "When Tony Stark tries to jumpstart a dormant peacekeeping program, things go awry and Earth’s Mightiest Heroes are put to the ultimate test as the fate of the planet hangs in the balance. As the villainous Ultron emerges, it is up to The Avengers to stop him from enacting his terrible plans, and soon uneasy alliances and unexpected action pave the way for an epic and unique global adventure.",
                "poster_path": "/4ssDuvEDkSArWEdyBl2X5EHvYKU.jpg",
                "release_date": "2015-04-22",
                "title": "Avengers: Age of Ultron",
                "video": false,
                "vote_average": 7.3,
                "vote_count": 17139
            }
        ],
        "known_for_department": "Acting",
        "name": "Elizabeth Olsen",
        "popularity": 26.935,
        "profile_path": "/wIU675y4dofIDVuhaNWPizJNtep.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 3223,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/kwUQFeFXOOpgloMgZaadhzkbTI4.jpg",
                "genre_ids": [
                    878,
                    28,
                    12
                ],
                "id": 24428,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Avengers",
                "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
                "poster_path": "/RYMX2wcKCBAr24UyPD7xwmjaTn.jpg",
                "release_date": "2012-04-25",
                "title": "The Avengers",
                "video": false,
                "vote_average": 7.7,
                "vote_count": 23829
            },
            {
                "adult": false,
                "backdrop_path": "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
                "genre_ids": [
                    12,
                    28,
                    878
                ],
                "id": 299536,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Avengers: Infinity War",
                "overview": "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
                "poster_path": "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
                "release_date": "2018-04-25",
                "title": "Avengers: Infinity War",
                "video": false,
                "vote_average": 8.3,
                "vote_count": 20663
            },
            {
                "adult": false,
                "backdrop_path": "/vbY95t58MDArtyUXUIb8Fx1dCry.jpg",
                "genre_ids": [
                    28,
                    878,
                    12
                ],
                "id": 1726,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Iron Man",
                "overview": "After being held captive in an Afghan cave, billionaire engineer Tony Stark creates a unique weaponized suit of armor to fight evil.",
                "poster_path": "/78lPtwv72eTNqFW9COBYI0dWDJa.jpg",
                "release_date": "2008-04-30",
                "title": "Iron Man",
                "video": false,
                "vote_average": 7.6,
                "vote_count": 19739
            }
        ],
        "known_for_department": "Acting",
        "name": "Robert Downey Jr.",
        "popularity": 26.249,
        "profile_path": "/im9SAqJPZKEbVZGmjXuLI4O7RvM.jpg"
    },
    {
        "adult": true,
        "gender": 1,
        "id": 2093355,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/saiXZOzwSIZEw7N1N8cKaqsBgJ.jpg",
                "genre_ids": [
                    10749
                ],
                "id": 569715,
                "media_type": "movie",
                "original_language": "ko",
                "original_title": "맛있는 세자매",
                "overview": "The gentle gesture of the three sisters surrounding the male housekeeper begins. The three sisters are not the food of the housemaid but the housekeeper's body.",
                "poster_path": "/q85VR4nlUUu9tERLUvPFyq2WB8c.jpg",
                "release_date": "2018-10-04",
                "title": "Delicious Three Sisters",
                "video": false,
                "vote_average": 4,
                "vote_count": 6
            },
            {
                "adult": false,
                "backdrop_path": "/58mXkzQqGAKUZeyWlTYVedaAO6Y.jpg",
                "genre_ids": [
                    10749
                ],
                "id": 560238,
                "media_type": "movie",
                "original_language": "ko",
                "original_title": "옆집소녀2",
                "overview": "Her husband Sejin is not interested in his wife Ahae as a woman. Ahae's husband is just heartless. In fact, Sejin is having an affair with Ara's young and beautiful younger brother Ara. After having suffered from depression due to work poisoning, he receives a message from the alumni association, and he changes his mind and attends. There, she meets Ahae, the next-door sister who had a crush on her childhood, and is still fantastic with her pretty appearance. Lonely Aha due to the innocence of her husband and Gunhoo approaching her with counseling. As if they were aware of each other's loneliness, they are attracted to each other and spend one night, returning to each other's daily lives, but after forgetting about the Ahae, the hand of the Ahae Abandonment comes to mind all night. Will they be able to go back to their daily lives?",
                "poster_path": "/cNk72g9PqEufTJcrNcch3SdQKxE.jpg",
                "release_date": "2018-12-14",
                "title": "The Girl Next Door 2",
                "video": false,
                "vote_average": 9,
                "vote_count": 2
            },
            {
                "adult": false,
                "backdrop_path": "/ooGMHxjhj2obfQGkw88UeBxUvdp.jpg",
                "genre_ids": [
                    10749
                ],
                "id": 538335,
                "media_type": "movie",
                "original_language": "ko",
                "original_title": "엄마의 유혹",
                "overview": "Jung-sik helps out a friend and finds himself attracted to his friend's mother. After being caught steeling the mother's underwear, he becomes the victim of the mother's seduction.",
                "poster_path": "/4dSNXqyXoFh8HbFkMnaKm2us06e.jpg",
                "release_date": "2018-07-31",
                "title": "Mother's Seduction",
                "video": false,
                "vote_average": 4,
                "vote_count": 2
            }
        ],
        "known_for_department": "Acting",
        "name": "Yoo Sul-young",
        "popularity": 26.031,
        "profile_path": null
    },
    {
        "adult": false,
        "gender": 1,
        "id": 4494,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/vZh2ZoBMyAiH8vYGqadyMBX50Gc.jpg",
                "genre_ids": [
                    14,
                    35
                ],
                "id": 310,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Bruce Almighty",
                "overview": "Bruce Nolan toils as a 'human interest' television reporter in Buffalo, N.Y., but despite his high ratings and the love of his beautiful girlfriend, Bruce remains unfulfilled. At the end of the worst day in his life, he angrily ridicules God—and the Almighty responds, endowing Bruce with all of His divine powers.",
                "poster_path": "/nF9hly6Dk2pcQFSnuxugI34RpCC.jpg",
                "release_date": "2003-05-23",
                "title": "Bruce Almighty",
                "video": false,
                "vote_average": 6.7,
                "vote_count": 8129
            },
            {
                "adult": false,
                "backdrop_path": "/vwCc9PP6xkSjnYsSl9lzTABhexe.jpg",
                "genre_ids": [
                    12,
                    53,
                    878
                ],
                "id": 74,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "War of the Worlds",
                "overview": "Ray Ferrier is a divorced dockworker and less-than-perfect father. Soon after his ex-wife and her new husband drop off his teenage son and young daughter for a rare weekend visit, a strange and powerful lightning storm touches down.",
                "poster_path": "/6Biy7R9LfumYshur3YKhpj56MpB.jpg",
                "release_date": "2005-06-28",
                "title": "War of the Worlds",
                "video": false,
                "vote_average": 6.4,
                "vote_count": 5895
            },
            {
                "adult": false,
                "backdrop_path": "/mrYJ0ijgaxjHKAs0ybNYxyfP8l5.jpg",
                "genre_ids": [
                    35,
                    10751
                ],
                "id": 9820,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Parent Trap",
                "overview": "Hallie Parker and Annie James are identical twins separated at a young age because of their parents' divorce. unknowingly to their parents, the girls are sent to the same summer camp where they meet, discover the truth about themselves, and then plot with each other to switch places. Hallie meets her mother, and Annie meets her father for the first time in years.",
                "poster_path": "/a3XOGrrAjl1wwiumtACWBufg6AL.jpg",
                "release_date": "1998-07-28",
                "title": "The Parent Trap",
                "video": false,
                "vote_average": 7.1,
                "vote_count": 2684
            }
        ],
        "known_for_department": "Acting",
        "name": "Lisa Ann Walter",
        "popularity": 25.614,
        "profile_path": "/3xeMzT5zOC0UM9DgHne3oGZmf1R.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 18918,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/rz3TAyd5kmiJmozp3GUbYeB5Kep.jpg",
                "genre_ids": [
                    28,
                    12,
                    35,
                    14
                ],
                "id": 353486,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Jumanji: Welcome to the Jungle",
                "overview": "The tables are turned as four teenagers are sucked into Jumanji's world - pitted against rhinos, black mambas and an endless variety of jungle traps and puzzles. To survive, they'll play as characters from the game.",
                "poster_path": "/22hqf97LadMvkd4zDi3Bq25xSqD.jpg",
                "release_date": "2017-12-09",
                "title": "Jumanji: Welcome to the Jungle",
                "video": false,
                "vote_average": 6.8,
                "vote_count": 10188
            },
            {
                "adult": false,
                "backdrop_path": "/oWU6dgu3RgdSZElkhbZuoPtkWAJ.jpg",
                "genre_ids": [
                    12,
                    35,
                    10751,
                    16
                ],
                "id": 277834,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Moana",
                "overview": "In Ancient Polynesia, when a terrible curse incurred by Maui reaches an impetuous Chieftain's daughter's island, she answers the Ocean's call to seek out the demigod to set things right.",
                "poster_path": "/vNJFiwtsl7Twww7C0uM4qPnygNw.jpg",
                "release_date": "2016-11-23",
                "title": "Moana",
                "video": false,
                "vote_average": 7.6,
                "vote_count": 9079
            },
            {
                "adult": false,
                "backdrop_path": "/mRfI3y2oAd7ejur2di09xC9niqp.jpg",
                "genre_ids": [
                    28,
                    53,
                    80
                ],
                "id": 82992,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Fast & Furious 6",
                "overview": "Hobbs has Dominic and Brian reassemble their crew to take down a team of mercenaries: Dominic unexpectedly gets convoluted also facing his presumed deceased girlfriend, Letty.",
                "poster_path": "/n31VRDodbaZxkrZmmzyYSFNVpW5.jpg",
                "release_date": "2013-05-21",
                "title": "Fast & Furious 6",
                "video": false,
                "vote_average": 6.8,
                "vote_count": 8362
            }
        ],
        "known_for_department": "Acting",
        "name": "Dwayne Johnson",
        "popularity": 24.777,
        "profile_path": "/kuqFzlYMc2IrsOyPznMd1FroeGq.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 73968,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/doiUtOHzcxXFl0GVQ2n8Ay6Pirx.jpg",
                "genre_ids": [
                    28,
                    12,
                    14
                ],
                "id": 209112,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Batman v Superman: Dawn of Justice",
                "overview": "Fearing the actions of a god-like Super Hero left unchecked, Gotham City’s own formidable, forceful vigilante takes on Metropolis’s most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than it’s ever known before.",
                "poster_path": "/5UsK3grJvtQrtzEgqNlDljJW96w.jpg",
                "release_date": "2016-03-23",
                "title": "Batman v Superman: Dawn of Justice",
                "video": false,
                "vote_average": 5.9,
                "vote_count": 14182
            },
            {
                "adult": false,
                "backdrop_path": "/69EFgWWPFWbRNHmQgYdSnyJ94Ge.jpg",
                "genre_ids": [
                    28,
                    12,
                    14,
                    878
                ],
                "id": 49521,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Man of Steel",
                "overview": "A young boy learns that he has extraordinary powers and is not of this earth. As a young man, he journeys to discover where he came from and what he was sent here to do. But the hero in him must emerge if he is to save the world from annihilation and become the symbol of hope for all mankind.",
                "poster_path": "/7rIPjn5TUK04O25ZkMyHrGNPgLx.jpg",
                "release_date": "2013-06-12",
                "title": "Man of Steel",
                "video": false,
                "vote_average": 6.6,
                "vote_count": 11660
            },
            {
                "adult": false,
                "backdrop_path": "/jorgjEk6a0bed6jdR5wu4S6ZvRm.jpg",
                "genre_ids": [
                    28,
                    12,
                    14,
                    878
                ],
                "id": 141052,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Justice League",
                "overview": "Fuelled by his restored faith in humanity and inspired by Superman's selfless act, Bruce Wayne and Diana Prince assemble a team of metahumans consisting of Barry Allen, Arthur Curry and Victor Stone to face the catastrophic threat of Steppenwolf and the Parademons who are on the hunt for three Mother Boxes on Earth.",
                "poster_path": "/eifGNCSDuxJeS1loAXil5bIGgvC.jpg",
                "release_date": "2017-11-15",
                "title": "Justice League",
                "video": false,
                "vote_average": 6.2,
                "vote_count": 10015
            }
        ],
        "known_for_department": "Acting",
        "name": "Henry Cavill",
        "popularity": 24.6,
        "profile_path": "/hErUwonrQgY5Y7RfxOfv8Fq11MB.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 42715,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/yKU6MtyiXqoE7mockHfvoLknYaa.jpg",
                "genre_ids": [
                    18
                ],
                "id": 14,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "American Beauty",
                "overview": "Lester Burnham, a depressed suburban father in a mid-life crisis, decides to turn his hectic life around after developing an infatuation with his daughter's attractive friend.",
                "poster_path": "/wby9315QzVKdW9BonAefg8jGTTb.jpg",
                "release_date": "1999-09-15",
                "title": "American Beauty",
                "video": false,
                "vote_average": 8,
                "vote_count": 8834
            },
            {
                "backdrop_path": "/l0qVZIpXtIo7km9u5Yqh0nKPOr5.jpg",
                "first_air_date": "1994-09-22",
                "genre_ids": [
                    35,
                    18
                ],
                "id": 1668,
                "media_type": "tv",
                "name": "Friends",
                "origin_country": [
                    "US"
                ],
                "original_language": "en",
                "original_name": "Friends",
                "overview": "The misadventures of a group of friends as they navigate the pitfalls of work, life and love in Manhattan.",
                "poster_path": "/f496cm9enuEsZkSPzCwnTESEK5s.jpg",
                "vote_average": 8.3,
                "vote_count": 3808
            },
            {
                "adult": false,
                "backdrop_path": "/ygYvhIsbOT3jwACwthDPIUzqXYK.jpg",
                "genre_ids": [
                    80,
                    53
                ],
                "id": 2118,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "L.A. Confidential",
                "overview": "Three detectives in the corrupt and brutal L.A. police force of the 1950s use differing methods to uncover a conspiracy behind the shotgun slayings of the patrons at an all-night diner.",
                "poster_path": "/Rzope4Pk93Rg1Q2Ae8H0FYwa7n.jpg",
                "release_date": "1997-09-19",
                "title": "L.A. Confidential",
                "video": false,
                "vote_average": 7.8,
                "vote_count": 3135
            }
        ],
        "known_for_department": "Acting",
        "name": "Amber Smith",
        "popularity": 21.005,
        "profile_path": "/6zUCOaxoRvIZLzDWbWWLOgeZMrf.jpg"
    },
    {
        "adult": false,
        "gender": 1,
        "id": 11701,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/xjotE7aFdZ0D8aGriYjFOtDayct.jpg",
                "genre_ids": [
                    14,
                    12,
                    28,
                    10751,
                    10749
                ],
                "id": 102651,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Maleficent",
                "overview": "A beautiful, pure-hearted young woman, Maleficent has an idyllic life growing up in a peaceable forest kingdom, until one day when an invading army threatens the harmony of the land.  Maleficent rises to be the land's fiercest protector, but she ultimately suffers a ruthless betrayal – an act that begins to turn her heart into stone. Bent on revenge, Maleficent faces an epic battle with the invading King's successor and, as a result, places a curse upon his newborn infant Aurora. As the child grows, Maleficent realizes that Aurora holds the key to peace in the kingdom – and to Maleficent's true happiness as well.",
                "poster_path": "/ik8PugpL41s137RAWEGTAWu0dPo.jpg",
                "release_date": "2014-05-28",
                "title": "Maleficent",
                "video": false,
                "vote_average": 7.1,
                "vote_count": 10576
            },
            {
                "adult": false,
                "backdrop_path": "/qdthf9WrRDSaIkGVQGhhJ9pz1hn.jpg",
                "genre_ids": [
                    12,
                    16,
                    10751,
                    35
                ],
                "id": 9502,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Kung Fu Panda",
                "overview": "When the Valley of Peace is threatened, lazy Po the panda discovers his destiny as the \"chosen one\" and trains to become a kung fu hero, but transforming the unsleek slacker into a brave warrior won't be easy. It's up to Master Shifu and the Furious Five -- Tigress, Crane, Mantis, Viper and Monkey -- to give it a try.",
                "poster_path": "/wWt4JYXTg5Wr3xBW2phBrMKgp3x.jpg",
                "release_date": "2008-06-04",
                "title": "Kung Fu Panda",
                "video": false,
                "vote_average": 7.1,
                "vote_count": 8055
            },
            {
                "adult": false,
                "backdrop_path": "/ka9sZqQfam7gv3evgNTXV1UmEFT.jpg",
                "genre_ids": [
                    28,
                    35,
                    18,
                    53
                ],
                "id": 787,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Mr. & Mrs. Smith",
                "overview": "After five (or six) years of vanilla-wedded bliss, ordinary suburbanites John and Jane Smith are stuck in a huge rut. Unbeknownst to each other, they are both coolly lethal, highly-paid assassins working for rival organisations. When they discover they're each other's next target, their secret lives collide in a spicy, explosive mix of wicked comedy, pent-up passion, nonstop action and high-tech weaponry.",
                "poster_path": "/wzIO3ytxeSNt1wRpXLIdkNbGoDm.jpg",
                "release_date": "2005-06-07",
                "title": "Mr. & Mrs. Smith",
                "video": false,
                "vote_average": 6.7,
                "vote_count": 7449
            }
        ],
        "known_for_department": "Acting",
        "name": "Angelina Jolie",
        "popularity": 20.924,
        "profile_path": "/tjJFdy3t5MnbWCl7LLGJgBXjhyJ.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 18897,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/qdthf9WrRDSaIkGVQGhhJ9pz1hn.jpg",
                "genre_ids": [
                    12,
                    16,
                    10751,
                    35
                ],
                "id": 9502,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Kung Fu Panda",
                "overview": "When the Valley of Peace is threatened, lazy Po the panda discovers his destiny as the \"chosen one\" and trains to become a kung fu hero, but transforming the unsleek slacker into a brave warrior won't be easy. It's up to Master Shifu and the Furious Five -- Tigress, Crane, Mantis, Viper and Monkey -- to give it a try.",
                "poster_path": "/wWt4JYXTg5Wr3xBW2phBrMKgp3x.jpg",
                "release_date": "2008-06-04",
                "title": "Kung Fu Panda",
                "video": false,
                "vote_average": 7.1,
                "vote_count": 8065
            },
            {
                "adult": false,
                "backdrop_path": "/iKJNE3QV0wEc9VMFuHYSZVhmSN9.jpg",
                "genre_ids": [
                    16,
                    10751
                ],
                "id": 49444,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Kung Fu Panda 2",
                "overview": "Po is now living his dream as The Dragon Warrior, protecting the Valley of Peace alongside his friends and fellow kung fu masters, The Furious Five - Tigress, Crane, Mantis, Viper and Monkey. But Po’s new life of awesomeness is threatened by the emergence of a formidable villain, who plans to use a secret, unstoppable weapon to conquer China and destroy kung fu. It is up to Po and The Furious Five to journey across China to face this threat and vanquish it. But how can Po stop a weapon that can stop kung fu? He must look to his past and uncover the secrets of his mysterious origins; only then will he be able to unlock the strength he needs to succeed.",
                "poster_path": "/mtqqD00vB4PGRt20gWtGqFhrkd0.jpg",
                "release_date": "2011-05-25",
                "title": "Kung Fu Panda 2",
                "video": false,
                "vote_average": 6.8,
                "vote_count": 4769
            },
            {
                "adult": false,
                "backdrop_path": "/y2Cl2LBYaabgtSLlOa4VqnOiNin.jpg",
                "genre_ids": [
                    28,
                    12,
                    18,
                    10751
                ],
                "id": 38575,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Karate Kid",
                "overview": "Twelve-year-old Dre Parker could have been the most popular kid in Detroit, but his mother's latest career move has landed him in China. Dre immediately falls for his classmate Mei Ying but the cultural differences make such a friendship impossible. Even worse, Dre's feelings make him an enemy of the class bully, Cheng. With no friends in a strange land, Dre has nowhere to turn but maintenance man Mr. Han, who is a kung fu master. As Han teaches Dre that kung fu is not about punches and parries, but maturity and calm, Dre realizes that facing down the bullies will be the fight of his life.",
                "poster_path": "/tAMQREOoztvluqrfHiGHFVfB04B.jpg",
                "release_date": "2010-06-10",
                "title": "The Karate Kid",
                "video": false,
                "vote_average": 6.4,
                "vote_count": 4210
            }
        ],
        "known_for_department": "Acting",
        "name": "Jackie Chan",
        "popularity": 20.545,
        "profile_path": "/nraZoTzwJQPHspAVsKfgl3RXKKa.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 2963,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/P4bGyJQF0shAcRku3I2perNUQR.jpg",
                "genre_ids": [
                    28,
                    80
                ],
                "id": 23483,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Kick-Ass",
                "overview": "Dave Lizewski is an unnoticed high school student and comic book fan who one day decides to become a super-hero, even though he has no powers, training or meaningful reason to do so.",
                "poster_path": "/GPiMXaDC2NdDIldLgtijHFTeKH.jpg",
                "release_date": "2010-03-22",
                "title": "Kick-Ass",
                "video": false,
                "vote_average": 7.1,
                "vote_count": 9076
            },
            {
                "adult": false,
                "backdrop_path": "/aUVCJ0HkcJIBrTJYPnTXta8h9Co.jpg",
                "genre_ids": [
                    28,
                    12,
                    16,
                    878,
                    35
                ],
                "id": 324857,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Spider-Man: Into the Spider-Verse",
                "overview": "Miles Morales is juggling his life between being a high school student and being a spider-man. When Wilson \"Kingpin\" Fisk uses a super collider, others from across the Spider-Verse are transported to this dimension.",
                "poster_path": "/iiZZdoQBEYBv6id8su7ImL0oCbD.jpg",
                "release_date": "2018-12-06",
                "title": "Spider-Man: Into the Spider-Verse",
                "video": false,
                "vote_average": 8.4,
                "vote_count": 8796
            },
            {
                "adult": false,
                "backdrop_path": "/7JCYI5Xrgt21fEQXIZ2ZFCRhyqK.jpg",
                "genre_ids": [
                    28,
                    12,
                    16,
                    35,
                    10751,
                    14
                ],
                "id": 49519,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Croods",
                "overview": "The prehistoric Croods family live in a particularly dangerous moment in time. Patriarch Grug, his mate Ugga, teenage daughter Eep, son Thunk, and feisty Gran gather food by day and huddle together in a cave at night. When a more evolved caveman named Guy arrives on the scene, Grug is distrustful, but it soon becomes apparent that Guy is correct about the impending destruction of their world.",
                "poster_path": "/27zvjVOtOi5ped1HSlJKNsKXkFH.jpg",
                "release_date": "2013-03-20",
                "title": "The Croods",
                "video": false,
                "vote_average": 6.9,
                "vote_count": 5135
            }
        ],
        "known_for_department": "Acting",
        "name": "Nicolas Cage",
        "popularity": 20.21,
        "profile_path": "/ar33qcWbEgREn07ZpXv5Pbj8hbM.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 9642,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/veXdzn7LL0bFIDGmE7tTkvRg0qV.jpg",
                "genre_ids": [
                    28,
                    12,
                    80,
                    9648
                ],
                "id": 10528,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Sherlock Holmes",
                "overview": "Eccentric consulting detective, Sherlock Holmes and Doctor John Watson battle to bring down a new nemesis and unravel a deadly plot that could destroy England.",
                "poster_path": "/momkKuWburNTqKBF6ez7rvhYVhE.jpg",
                "release_date": "2009-12-23",
                "title": "Sherlock Holmes",
                "video": false,
                "vote_average": 7.2,
                "vote_count": 11031
            },
            {
                "adult": false,
                "backdrop_path": "/w2PMyoyLU22YvrGK3smVM9fW1jj.jpg",
                "genre_ids": [
                    28,
                    12,
                    878
                ],
                "id": 299537,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Captain Marvel",
                "overview": "The story follows Carol Danvers as she becomes one of the universe’s most powerful heroes when Earth is caught in the middle of a galactic war between two alien races. Set in the 1990s, Captain Marvel is an all-new adventure from a previously unseen period in the history of the Marvel Cinematic Universe.",
                "poster_path": "/AtsgWhDnHTq68L0lLsUrCnM7TjG.jpg",
                "release_date": "2019-03-06",
                "title": "Captain Marvel",
                "video": false,
                "vote_average": 7,
                "vote_count": 10660
            },
            {
                "adult": false,
                "backdrop_path": "/wMQgG7pzfFoorL4JC0U9YKlkcSl.jpg",
                "genre_ids": [
                    35,
                    18
                ],
                "id": 120467,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Grand Budapest Hotel",
                "overview": "The Grand Budapest Hotel tells of a legendary concierge at a famous European hotel between the wars and his friendship with a young employee who becomes his trusted protégé. The story involves the theft and recovery of a priceless Renaissance painting, the battle for an enormous family fortune and the slow and then sudden upheavals that transformed Europe during the first half of the 20th century.",
                "poster_path": "/eWdyYQreja6JGCzqHWXpWHDrrPo.jpg",
                "release_date": "2014-02-26",
                "title": "The Grand Budapest Hotel",
                "video": false,
                "vote_average": 8,
                "vote_count": 10565
            }
        ],
        "known_for_department": "Acting",
        "name": "Jude Law",
        "popularity": 20.084,
        "profile_path": "/A6Y0m7qEe04ZTHKyYDLbnyCHNzn.jpg"
    },
    {
        "adult": false,
        "gender": 2,
        "id": 1100,
        "known_for": [
            {
                "adult": false,
                "backdrop_path": "/hplKoKmi0IL1uDPYtHu3rogcvIQ.jpg",
                "genre_ids": [
                    28,
                    53,
                    878
                ],
                "id": 218,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "The Terminator",
                "overview": "In the post-apocalyptic future, reigning tyrannical supercomputers teleport a cyborg assassin known as the \"Terminator\" back to 1984 to kill Sarah Connor, whose unborn son is destined to lead insurgents against 21st century mechanical hegemony. Meanwhile, the human-resistance movement dispatches a lone warrior to safeguard Sarah. Can he stop the virtually indestructible killing machine?",
                "poster_path": "/qvktm0BHcnmDpul4Hz01GIazWPr.jpg",
                "release_date": "1984-10-26",
                "title": "The Terminator",
                "video": false,
                "vote_average": 7.6,
                "vote_count": 9008
            },
            {
                "adult": false,
                "backdrop_path": "/xKb6mtdfI5Qsggc44Hr9CCUDvaj.jpg",
                "genre_ids": [
                    28,
                    53,
                    878
                ],
                "id": 280,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Terminator 2: Judgment Day",
                "overview": "Nearly 10 years have passed since Sarah Connor was targeted for termination by a cyborg from the future. Now her son, John, the future leader of the resistance, is the target for a newer, more deadly terminator. Once again, the resistance has managed to send a protector back to attempt to save John and his mother Sarah.",
                "poster_path": "/weVXMD5QBGeQil4HEATZqAkXeEc.jpg",
                "release_date": "1991-07-03",
                "title": "Terminator 2: Judgment Day",
                "video": false,
                "vote_average": 8,
                "vote_count": 8891
            },
            {
                "adult": false,
                "backdrop_path": "/g4a5YLWwi6OCp8TcvxsUNrXMbN5.jpg",
                "genre_ids": [
                    878,
                    28,
                    53,
                    12
                ],
                "id": 87101,
                "media_type": "movie",
                "original_language": "en",
                "original_title": "Terminator Genisys",
                "overview": "The year is 2029. John Connor, leader of the resistance continues the war against the machines. At the Los Angeles offensive, John's fears of the unknown future begin to emerge when TECOM spies reveal a new plot by SkyNet that will attack him from both fronts; past and future, and will ultimately change warfare forever.",
                "poster_path": "/oZRVDpNtmHk8M1VYy1aeOWUXgbC.jpg",
                "release_date": "2015-06-23",
                "title": "Terminator Genisys",
                "video": false,
                "vote_average": 5.9,
                "vote_count": 6464
            }
        ],
        "known_for_department": "Acting",
        "name": "Arnold Schwarzenegger",
        "popularity": 20.069,
        "profile_path": "/tVm6EVs4xv5on3i9xG1gp8UQ1fs.jpg"
    }
]